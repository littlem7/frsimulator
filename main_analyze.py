#!/usr/bin/env python
from frsimulator.recorder import Recorder
from frsimulator.analyzer import Analyzer


def main():
    Analyzer(Recorder())

if __name__ == "__main__":
    main()
