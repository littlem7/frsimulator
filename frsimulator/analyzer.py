import matplotlib.pylab as plt
import numpy as np
import time
import re


class Analyzer:
    """
    types of lines on plots
    """
    plots_line = ["b", "r", "y", "g"]

    def __init__(self, recorder):
        self.path = recorder.log_files_path
        self.files = recorder.names_of_log_files
        self.log_separator = recorder.separator_pattern
        self.units = recorder.units
        self.data = {}
        self.quantity_of_x_dates = 4
        for name in self.files:
            self.data[name] = self.log_to_tuple(self.path + name)

        figure, plots = plt.subplots(len(self.files), sharex=True)
        figure.canvas.set_window_title("Flight recorder simulator - flight presentation")

        self.x_dates_init = True

        for i in range(0, len(self.files)):
            x = (self.data[self.files[i]])[0]
            y = (self.data[self.files[i]])[1]
            """
            Making x_dates on plot
            """
            if self.x_dates_init:
                if len(x) > self.quantity_of_x_dates:
                    x_dates = []
                    x_raw_ticks = np.arange(x[0], x[-1]+0.1, (x[-1]-x[0])/self.quantity_of_x_dates)
                    for raw in x_raw_ticks:
                        time_tuple = time.localtime(raw)
                        x_dates.append(time.strftime('%Y-%m-%d %H:%M:%S', time_tuple))
                    plt.xticks(x_raw_ticks, x_dates)
                    plt.grid(True)
                    self.x_dates_init = False

            if i == len(self.files)-1:
                plots[i].scatter(x, y)
                plots[i].set_xlabel("Date [y-m-d h:m:s]")
                plots[i].get_yaxis().set_visible(False)
            else:
                plots[i].plot(x, y, self.plots_line[i % len(self.plots_line)])
                plots[i].set_ylabel(self.units[i])
            plots[i].grid(True)
            """
            Names for plots
            """
            raw_title = re.search('([\w+ +]+)', self.files[i])
            title_list = list(raw_title.group(1))
            title_list[0] = title_list[0].upper()
            plots[i].set_title(''.join(title_list))
        mng = plt.get_current_fig_manager()
        mng.resize(*mng.window.maxsize())
        plt.show()

    @staticmethod
    def to_float(value):
        try:
            return float(value)
        except ValueError:
            return 0

    @staticmethod
    def to_int(value):
        try:
            return int(value)
        except ValueError:
            return 0

    @staticmethod
    def date_to_float(date):
        match = re.findall('(\d+)', date,)
        to_time_tuple = []
        for i in range(0, 6):
            to_time_tuple.append(Analyzer.to_int(match[i]))
        for i in range(7, 10):
            to_time_tuple.append(-1)
        return time.mktime(tuple(to_time_tuple))

    @staticmethod
    def log_to_tuple(file):
        the_file = open(file, 'r')
        reader_file = the_file.readlines()
        the_file.close()
        x = []
        y = []
        for line in reader_file:
            separate = re.search('([\d-]+ \d+[:\d+]+,\d+) : ([\D+\d+]+)\\n', line)
            x.append(Analyzer.date_to_float(separate.group(1)))
            y.append(Analyzer.to_float(separate.group(2)))
        return x, y
