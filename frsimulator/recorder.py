import logging
import os


class Recorder:
    def __init__(self):
        self.separator_pattern = ' : '
        log_format = logging.Formatter('%(asctime)s' + self.separator_pattern + '%(message)s')
        self.log_files_path = os.path.dirname(os.path.abspath(__file__)) + "/../"
        """
        You can add your own value file before 'bad frames.log'
        An you should take care of units
        """
        self.names_of_log_files = ('altitude.log', 'airspeed.log', 'pitch.log', 'roll.log', 'bad frames.log')
        self.units = ('[km]', '[km/h]', '[°]', '[°]')
        help_list_of_files = []
        for name, i in zip(self.names_of_log_files, range(0, len(self.names_of_log_files))):
            help_list_of_files.append(logging.getLogger(name))
            help_list_of_files[i].setLevel(logging.INFO)
            handler = logging.FileHandler(self.log_files_path + name)
            handler.setLevel(logging.INFO)
            handler.setFormatter(log_format)
            help_list_of_files[i].addHandler(handler)
        """
        Tuple - can't modify log_files tuple
        """
        self.log_files = tuple(help_list_of_files)

    def record_values(self, *values):
        """
        :param values: tuple of values to fill in log files
        """
        out_of_range = len(self.log_files)-1
        if len(values) < out_of_range:
            self.log_files[out_of_range].info(values)
        for value, i in zip(values, range(0, len(self.log_files))):
            """
            Requirement for too many values
            """
            if i == out_of_range:
                self.log_files[out_of_range].info(values[4:])
                return
            else:
                self.log_files[i].info(value)