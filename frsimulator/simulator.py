from frsimulator.recorder import Recorder
from time import sleep
import numpy as np
import math


class Simulator:
    max_airspeed_to_land = 200  # [km/h]
    min_airspeed_to_fly = 270   # [km/h]
    max_airspeed = 900          # [km/h]
    acceleration = 30000        # [km/h^2]
    sec = 1/3600                # [h]
    max_positive_pitch = 30     # [°]
    max_negative_pitch = -30    # [°]
    stabilize_pitch = 0.01      # [°]
    my_sleep = 1

    def __init__(self):
        self.recorder = Recorder()
        self.current_altitude = 0
        self.current_airspeed = 0
        self.current_pitch = 1
        self.current_roll = 0.1

    def simply_flight(self, total_time, high):
        # let's fly
        print("Simply flight - init")
        self.flight_initiation(high)
        print("Simply flight - normal flight")
        self.calm_flight(total_time, high)
        print("Simply flight - landing")
        self.landing()

    def flight_initiation(self, high):
        while self.current_altitude < high:
            if self.current_airspeed < Simulator.min_airspeed_to_fly:    # plane acceleration in the ground
                self.current_airspeed += Simulator.acceleration * Simulator.sec
            else:                                                       # plane acceleration in the air
                self.current_altitude += self.current_airspeed * math.sin(self.current_pitch*math.pi/180) * Simulator.sec
                if self.current_airspeed < Simulator.max_airspeed:
                    self.current_airspeed = Simulator.fluctuate(self.current_airspeed, "+")
                else:
                    self.current_airspeed = Simulator.fluctuate(self.current_airspeed, '-')
                if self.current_pitch > Simulator.max_positive_pitch:
                    self.current_pitch = Simulator.fluctuate(self.current_pitch, "-")
                else:
                    self.current_pitch = Simulator.fluctuate(self.current_pitch, "+")
                self.current_roll = Simulator.fluctuate(self.current_roll)
            sleep(Simulator.my_sleep)
            print("STARTING: \taltitude: %.2f \tairspeed: %.2f \tpitch: %.2f \troll: %.2f" % (self.current_altitude, self.current_airspeed, self.current_pitch, self.current_roll))
            self.recorder.record_values(self.current_altitude, self.current_airspeed, self.current_pitch, self.current_roll)

    def calm_flight(self, time, high):
        for i in range(0, time):
            self.current_altitude += self.current_airspeed * math.sin(self.current_pitch*math.pi/180) * Simulator.sec

            if self.current_altitude > high:
                if -Simulator.stabilize_pitch > self.current_pitch:
                    self.current_pitch = Simulator.fluctuate(self.current_pitch, "+")
                elif self.current_pitch > -Simulator.stabilize_pitch:
                    self.current_pitch = Simulator.fluctuate(self.current_pitch, "-")
            else:
                if -Simulator.stabilize_pitch > self.current_pitch:
                    self.current_pitch = Simulator.fluctuate(self.current_pitch, "+")
                elif self.current_pitch > -Simulator.stabilize_pitch:
                    self.current_pitch = Simulator.fluctuate(self.current_pitch, "-")
            if self.current_airspeed > Simulator.max_airspeed:
                self.current_airspeed = Simulator.fluctuate(self.current_airspeed, "-")
            else:
                self.current_airspeed = Simulator.fluctuate(self.current_airspeed, "+")
            self.current_roll = Simulator.fluctuate(self.current_roll)

            sleep(Simulator.my_sleep)
            print("FLIGHT: \taltitude: %.2f \tairspeed: %.2f \tpitch: %.2f \troll: %.2f" % (self.current_altitude, self.current_airspeed, self.current_pitch, self.current_roll))
            self.recorder.record_values(self.current_altitude, self.current_airspeed, self.current_pitch, self.current_roll)

    def landing(self):
        while self.current_airspeed > 0:
            if self.current_altitude > 0:       # plane acceleration in the air
                self.current_altitude += self.current_airspeed * math.sin(self.current_pitch*math.pi/180) * Simulator.sec
                if Simulator.max_airspeed_to_land < self.current_airspeed < Simulator.min_airspeed_to_fly:
                    if self.current_pitch > Simulator.max_negative_pitch:
                        self.current_pitch = Simulator.fluctuate(self.current_pitch, "-")
                    else:
                        self.current_pitch = Simulator.fluctuate(self.current_pitch, "+")
                    self.current_airspeed = Simulator.fluctuate(self.current_airspeed)
                elif self.current_airspeed > Simulator.min_airspeed_to_fly:
                    self.current_airspeed = Simulator.fluctuate(self.current_airspeed, "-")
                else:
                    self.current_airspeed = Simulator.fluctuate(self.current_airspeed, "+")
                    if self.current_pitch > Simulator.max_negative_pitch:
                        self.current_pitch = Simulator.fluctuate(self.current_pitch, "-")
                    else:
                        self.current_pitch = Simulator.fluctuate(self.current_pitch, "+")
                    self.current_airspeed = Simulator.fluctuate(self.current_airspeed)
                self.current_roll = Simulator.fluctuate(self.current_roll)
            else:                               # plane acceleration in the ground - braking
                self.current_altitude = 0
                if self.current_pitch < 1:
                    self.current_pitch = Simulator.fluctuate(self.current_pitch, "+")
                else:
                    self.current_pitch = Simulator.fluctuate(self.current_pitch, "-")
                self.current_roll = 0
                self.current_airspeed -= Simulator.acceleration * Simulator.sec
            sleep(Simulator.my_sleep)
            print("LANDING: \taltitude: %.2f \tairspeed: %.2f \tpitch: %.2f \troll: %.2f" % (self.current_altitude, self.current_airspeed, self.current_pitch, self.current_roll))
            self.recorder.record_values(self.current_altitude, self.current_airspeed, self.current_pitch, self.current_roll)

    @staticmethod
    def fluctuate(value, sign = ''):
        random = np.random.rand()/10
        if sign == '+':
            if value > 0:
                value += random*value
            else:
                value -= random*value
            return value
        elif sign == '-':
            if value > 0.1:
                value -= random*value
            elif 0.1 >= value >= -0.1:
                value -= 0.11
            else:
                value += random*value
            return value
        else:
            return Simulator.fluctuate(value, np.random.choice(['+', '-']))