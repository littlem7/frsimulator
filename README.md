# Full description in Sprawozdanie.pdf file #

### Makefile commands ###

```
#!python

make test_recorder
```
 - creating one black box and sending a few random correct and incorrect values
(should wait 10 seconds);

```
#!python

make analyze
```
 - allows you to run an analysis from created .log files with data flight parameters;

```
#!python

make fly
```
 - allows you to create a flight simulator of default set time flight 100 seconds and default set a rate of 20 km (should wait additional time to starting and landing; about 13 minutes for this setting)

```
#!python

make clean
```
 - deletes all .log files with data collected during the flight