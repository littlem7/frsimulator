#!/usr/bin/env python
from frsimulator.analyzer import Analyzer
from frsimulator.simulator import Simulator


def main():
    simulator = Simulator()
    simulator.simply_flight(100, 20)
    Analyzer(simulator.recorder)

if __name__ == "__main__":
    main()
