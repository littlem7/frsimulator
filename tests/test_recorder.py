from frsimulator.recorder import Recorder
from frsimulator.analyzer import Analyzer
from time import sleep


def test_recorder():
    """
    Testing good and bad input in reader
    """
    plane_recorder = Recorder()
    for i in range(1, 10):
        """
        Good data in proper files
        """
        plane_recorder.record_values(i, i, i, i)
        """
        Data in another_info.log
        """
        if i%7 == 0:
            plane_recorder.record_values(i+10, i+10, i+10)
        """
        Delay in recording
        """
        sleep(1)
        print(i)
    Analyzer(plane_recorder)
