from frsimulator.simulator import Simulator
from frsimulator.analyzer import Analyzer


def test_simulator():
    simulator = Simulator()
    simulator.simply_flight(10, 0.1)
    Analyzer(simulator.recorder)
