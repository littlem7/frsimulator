from setuptools import setup

setup(
    name='frsimulator',
    version='1.0',
    description='Flight recorder simulator',
    author='Anna Bogusz',
    author_email='anna.bogusz@fis.agh.edu.pl',
    packages=[
        'frsimulator',
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest'
    ],
    install_requires=[
        'pytest',
        'matplotlib',
    ],
)
